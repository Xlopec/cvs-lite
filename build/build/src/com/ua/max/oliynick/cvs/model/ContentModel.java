package com.ua.max.oliynick.cvs.model;

import com.ua.max.oliynick.cvs.interfaces.ContentFactory;
import com.ua.max.oliynick.cvs.interfaces.Model;

public class ContentModel implements Model {

	private static ContentFactory contentFactory;//TODO Implement
	
	public static ContentFactory getContentFactory() {
		return ContentModel.contentFactory;
	}
	
	public static void setContentFactory(final ContentFactory contentFactory) {
		ContentModel.contentFactory = contentFactory;
	}
	
}
