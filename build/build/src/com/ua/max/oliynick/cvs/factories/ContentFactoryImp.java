package com.ua.max.oliynick.cvs.factories;

import java.io.IOException;
import java.util.ResourceBundle;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.util.Callback;

import com.ua.max.oliynick.cvs.controller.FileContentController;
import com.ua.max.oliynick.cvs.interfaces.Adjustable;
import com.ua.max.oliynick.cvs.interfaces.ContentFactory;
import com.ua.max.oliynick.cvs.interfaces.Controller;
import com.ua.max.oliynick.cvs.interfaces.Options;
import com.ua.max.oliynick.cvs.interfaces.View;
import com.ua.max.oliynick.cvs.model.FileContentModel;
import com.ua.max.oliynick.cvs.utils.GeneralPreferences;
import com.ua.max.oliynick.cvs.utils.OptionsManager;

public class ContentFactoryImp implements ContentFactory, Adjustable<Options> {
	
	private final GeneralPreferences generalPrefs;
	
	private final ResourceBundle fileViewRes;
	
	public ContentFactoryImp() {
		generalPrefs = OptionsManager.getGeneralOptions();
		fileViewRes = ResourceBundle.getBundle("resources/locales/ban", generalPrefs.getLocale());
	}

	@Override
	public View createFileView() {
		
		FXMLLoader loader = new FXMLLoader(ContentFactoryImp.class.getClassLoader().getResource("resources/fxml/FileContent.fxml"), fileViewRes);
		loader.setControllerFactory(new Callback<Class<?>, Object>() {
			
			@Override
			public Object call(Class<?> param) {
				return new FileContentController(new FileContentModel());
			}
		});
		
		try {
			Node node = loader.load();
			Controller<FileContentModel> c = loader.getController();
			
			//TODO internationalize
			return new View(c, node);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
	}

	@Override
	public View createSearchResView() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void adjust(Options prefs) {
		//TODO implement
	}

}
