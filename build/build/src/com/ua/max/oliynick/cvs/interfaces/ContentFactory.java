package com.ua.max.oliynick.cvs.interfaces;

public interface ContentFactory {
	
	public View createFileView();
	
	public View createSearchResView();

}
