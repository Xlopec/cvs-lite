package com.ua.max.oliynick.cvs.interfaces;

public interface Adjustable <T extends Options> {
	
	public void adjust(final T t);

}
