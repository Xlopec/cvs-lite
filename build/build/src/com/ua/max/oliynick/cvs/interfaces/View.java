package com.ua.max.oliynick.cvs.interfaces;

import javafx.scene.Node;
import javafx.scene.image.Image;

public class View {
	
	private String title;
	
	private final Object id;
	
	private final Controller<?> controller;
	
	private final Model model;
	
	private Image icon;
	
	private final Node root;
	
	public View(final Controller<?> c, final Node root) {
		this(null, null, c, root, null);
	}
	
	public View(final String title, final Object id, final Controller<?> c, final Node root) {
		this(title, id, c, root, null);
	}
	
	public View(final String title, final Object id, final Controller<?> c, final Node root, final Image icon) {
		this.title = title;
		this.id = null;
		this.controller = c;
		this.model = c.getModel();
		this.root = root;
		this.icon = icon;
	}
	
	public void setTitle(final String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
	
	public Object getId() {
		return id;
	}
	
	public Model getModel() {
		return model;
	}

	public Node getRoot() {
		return root;
	}
	
	public Image getIcon() {
		return icon;
	}

	public Controller<?> getController() {
		return controller;
	}
	
}
