package com.ua.max.oliynick.cvs.launcher;

import java.util.Locale;
import java.util.ResourceBundle;

import com.ua.max.oliynick.cvs.factories.ContentFactoryImp;
import com.ua.max.oliynick.cvs.interfaces.ContentFactory;
import com.ua.max.oliynick.cvs.interfaces.View;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Launcher extends Application {
	
	@Override
	public void start(final Stage primaryStage) {
		
		Platform.setImplicitExit(true);
		Locale.setDefault(Locale.ENGLISH);
		
		ContentFactory factory = new ContentFactoryImp();
		View v = factory.createFileView();
		
		StackPane sp = new StackPane(v.getRoot());
		
		Scene scene = new Scene(sp);
		primaryStage.setScene(scene);
		primaryStage.setTitle("test");
		primaryStage.show();
		
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
