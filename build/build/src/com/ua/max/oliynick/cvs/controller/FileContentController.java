package com.ua.max.oliynick.cvs.controller;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.ListView;

import com.ua.max.oliynick.cvs.interfaces.Controller;
import com.ua.max.oliynick.cvs.model.FileContentModel;

public class FileContentController extends Controller<FileContentModel> {
	
	@FXML private ListView<Parent> data;

	public FileContentController(FileContentModel model) {
		super(model);
	}

}
