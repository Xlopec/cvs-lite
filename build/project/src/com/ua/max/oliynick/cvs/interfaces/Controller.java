package com.ua.max.oliynick.cvs.interfaces;

public class Controller<M extends Model> {
	
	private final M model;
	
	public Controller(final M model) {
		this.model = model;
	}
	
	public M getModel() {
		return model;
	}
	
}
