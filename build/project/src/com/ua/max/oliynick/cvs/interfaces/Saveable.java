package com.ua.max.oliynick.cvs.interfaces;

public interface Saveable {
	
	public void save() throws Exception;

}
