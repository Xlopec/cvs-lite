package com.ua.max.oliynick.cvs.interfaces;

public abstract class ContentController<T extends Model> extends Controller<T> {
	
	public ContentController(final T model) {
		super(model);
	}

	public abstract void close();
	
	public abstract void clear();

}
