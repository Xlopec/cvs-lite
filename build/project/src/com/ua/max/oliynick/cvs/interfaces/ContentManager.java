package com.ua.max.oliynick.cvs.interfaces;

import java.util.Collection;

public interface ContentManager extends Adjustable {
	
	public boolean open(int indx);
	
	public boolean open(final View v);

	public boolean close(int indx);
	
	public boolean close(final View v);
	
	public void closeAll();
	
	public void restoreRecent();
	
	public void restoreAll();
	
	public Collection<View> getContent();
	
	public void init(final View v);
	
	public void putChildren(final View v);
}
