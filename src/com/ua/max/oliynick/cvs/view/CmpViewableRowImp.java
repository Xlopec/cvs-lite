package com.ua.max.oliynick.cvs.view;

import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

import com.ua.max.oliynick.cvs.interfaces.ViewableRow;
import com.ua.max.oliynick.cvs.utils.CmpRow;
import com.ua.max.oliynick.cvs.utils.CmpRow.Type;

public class CmpViewableRowImp extends ViewableRow<Parent, CmpRow> {
	
	public CmpViewableRowImp(CmpRow row) {
		super(row);
	}	

	@Override
	protected HBox buildView() {
		
		HBox root = new HBox(7);
		Region resizer = new Region();
		HBox.setHgrow(resizer, Priority.ALWAYS);
		
		CmpRow cmpRow = getRow();
		
		if(cmpRow.getIndx() > 0) {
			Label rowIndex = new Label(String.valueOf(cmpRow.getIndx()));
			HBox.setHgrow(rowIndex, Priority.NEVER);
			root.getChildren().add(rowIndex);
		}
		
		Label content = new Label(cmpRow.getContent());
		HBox.setHgrow(content, Priority.NEVER);
		
		root.getChildren().addAll(content, resizer);
		
		if(cmpRow.getStatus() == Type.DELETED) {
			content.getStyleClass().clear();
			content.getStyleClass().add("removed-row");
		} else if(cmpRow.getStatus() == Type.ADDED) {
			content.getStyleClass().clear();
			content.getStyleClass().add("added-row");
		}
		
		return root;
	}

}
