package com.ua.max.oliynick.cvs.view;

import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

import com.ua.max.oliynick.cvs.interfaces.ViewableRow;
import com.ua.max.oliynick.cvs.utils.Row;

public class ViewableRowImp extends ViewableRow<Parent, Row> {
	
	public ViewableRowImp(Row row) {
		super(row);
	}

	@Override
	protected HBox buildView() {
		
		HBox root = new HBox(7);
		Row cmpRow = getRow();
		
		if(cmpRow.getIndx() > 0) {
			root.getChildren().add(new Label(String.valueOf(cmpRow.getIndx())));
		}
		
		root.getChildren().add(new Label(cmpRow.getContent()));
		
		return root;
	}

}
