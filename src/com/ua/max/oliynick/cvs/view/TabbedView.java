package com.ua.max.oliynick.cvs.view;

import javafx.scene.Node;
import javafx.scene.control.Tab;

import com.ua.max.oliynick.cvs.interfaces.IController;
import com.ua.max.oliynick.cvs.interfaces.IView;
import com.ua.max.oliynick.cvs.interfaces.IModel;

public class TabbedView <M extends IModel, Cntrl extends IController> extends Tab implements IView <M, Cntrl> {
	
	private String title;
	
	private final String id;
	
	private final Cntrl controller;
	
	private final M model;
	
	private final Node root;
	
	public TabbedView(final Node root, final Cntrl c, final M m) {
		this(null, null, root, c, m);
	}
	
	public TabbedView(final String title, final Node root, final Cntrl c, final M m) {
		this(null, title, root, c, m);
	}
	
	public TabbedView(final String title, final String id, final Node root, final Cntrl c, final M m) {
		this.title = title;
		this.id = null;
		this.controller = c;
		this.model = m;
		this.root = root;
		this.setContent(root);
		this.setText(title);
	}
	
	public TabbedView(final IView<M, Cntrl> another) {
		this(another.getTitle(), another.getViewId(), another.getRoot(),
				another.getController(), another.getModel());
	}

	@Override
	public boolean equals(Object obj) {
		
		if(obj == null) return false;
		if(obj.hashCode() != hashCode()) return false;
		if(!(obj instanceof TabbedView<?, ?>)) return false;
		
		TabbedView<?, ?> tv = (TabbedView<?, ?>) obj;
		
		if(tv.getId() != null && getId() != null) {
			if(!tv.getId().equals(getId())) return false;
		}
		
		if(tv.getRoot() != null && getRoot() != null) {
			if(!tv.getRoot().equals(getRoot())) return false;
		}
		
		return super.equals(obj);
	}
	
	

	@Override
	public int hashCode() {
		int prime = 33;
		return prime * (id == null ? super.hashCode() : id.hashCode());
	}

	@Override
	public String getTitle() {
		return title;
	}
	
	@Override
	public String getViewId() {
		return id;
	}
	
	@Override
	public M getModel() {
		return model;
	}

	@Override
	public Node getRoot() {
		return root;
	}

	@Override
	public Cntrl getController() {
		return controller;
	}

	@Override
	public void setTitle(String title) {
		this.setText(title);
	}

}
