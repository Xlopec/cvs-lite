package com.ua.max.oliynick.cvs.factories;

import com.ua.max.oliynick.cvs.interfaces.FileContentComparator;

public class FileComparatorManager {
	
	private static FileContentComparator lcsComparator;
	
	private FileComparatorManager() {}
	
	public static FileContentComparator getDefaultComparator() {
		
		if(FileComparatorManager.lcsComparator == null) 
			FileComparatorManager.lcsComparator = new LCSFileComparator();
		
		return FileComparatorManager.lcsComparator;
	}

}
