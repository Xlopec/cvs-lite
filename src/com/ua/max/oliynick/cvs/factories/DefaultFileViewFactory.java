package com.ua.max.oliynick.cvs.factories;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.util.Callback;

import com.ua.max.oliynick.cvs.controller.CmpFileContentControllerImp;
import com.ua.max.oliynick.cvs.controller.FileContentControllerImp;
import com.ua.max.oliynick.cvs.interfaces.ICmpFileContentController;
import com.ua.max.oliynick.cvs.interfaces.ICmpFileContentModel;
import com.ua.max.oliynick.cvs.interfaces.IFileContentController;
import com.ua.max.oliynick.cvs.interfaces.FileContentModel;
import com.ua.max.oliynick.cvs.interfaces.FileViewFactory;
import com.ua.max.oliynick.cvs.interfaces.IView;
import com.ua.max.oliynick.cvs.interfaces.View;
import com.ua.max.oliynick.cvs.interfaces.ViewableRow;
import com.ua.max.oliynick.cvs.model.CmpFileContentModelImp;
import com.ua.max.oliynick.cvs.model.FileContentModelImp;
import com.ua.max.oliynick.cvs.utils.CmpRow;
import com.ua.max.oliynick.cvs.utils.GeneralPreferences;
import com.ua.max.oliynick.cvs.utils.OptionsManager;
import com.ua.max.oliynick.cvs.utils.Row;

public class DefaultFileViewFactory extends FileViewFactory {
	
	private static final GeneralPreferences generalPrefs;
	
	private static final ResourceBundle fileViewRes;
	
	//private static final URL fileContentHolderUrl;
	private static final URL fileContentUrl;
	
	static {
		generalPrefs = OptionsManager.getGeneralOptions();
		fileViewRes = ResourceBundle.getBundle("resources/locales/file_content", generalPrefs.getLocale());//TODO finish
		//fileContentHolderUrl = DefaultFileViewFactory.class.getClassLoader().getResource("resources/fxml/FileContentHolder.fxml");
		fileContentUrl = DefaultFileViewFactory.class.getClassLoader().getResource("resources/fxml/FileContent.fxml");
	}
	
	public DefaultFileViewFactory() {}

	@Override
	public IView<FileContentModel, IFileContentController> createContentView
	(List<ViewableRow<Parent, Row>> data, File file) {
		
		FXMLLoader loader = new FXMLLoader(fileContentUrl, fileViewRes);
		loader.setControllerFactory(new Callback<Class<?>, Object>() {
			
			@Override
			public Object call(Class<?> param) {
				return new FileContentControllerImp(new FileContentModelImp(data, file));
			}
		});
		
		try {
			Node node = loader.load();
			IFileContentController controller = loader.getController();
			
			return new View<FileContentModel, IFileContentController>(file.getName(), null, node, controller, controller.getModel());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
	}

	@Override
	public IView<ICmpFileContentModel, ICmpFileContentController> createCmpContentView
	(List<ViewableRow<Parent, CmpRow>> data, File file) {
		
		FXMLLoader loader = new FXMLLoader(fileContentUrl, fileViewRes);
		loader.setControllerFactory(new Callback<Class<?>, Object>() {
			
			@Override
			public Object call(Class<?> param) {
				return new CmpFileContentControllerImp(new CmpFileContentModelImp(data, file));
			}
		});
		
		try {
			Node node = loader.load();
			ICmpFileContentController controller = loader.getController();
			
			return new View<ICmpFileContentModel, ICmpFileContentController>(file.getName(), null, node, controller, controller.getModel());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

}
