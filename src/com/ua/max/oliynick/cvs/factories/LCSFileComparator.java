package com.ua.max.oliynick.cvs.factories;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.ua.max.oliynick.cvs.interfaces.FileContentComparator;
import com.ua.max.oliynick.cvs.utils.CompareResult;
import com.ua.max.oliynick.cvs.utils.CmpRow;
import com.ua.max.oliynick.cvs.utils.CmpRow.Type;

public class LCSFileComparator implements FileContentComparator {

	@Override
	public CompareResult compare(final String [] oldContent, final String [] newContent) {
		
		if(oldContent == null)
			throw new IllegalArgumentException("old content == null");
		
		if(newContent == null)
			throw new IllegalArgumentException("new content == null");
		
		int [][] lcpMatrix = LCSFileComparator.createMatrix(oldContent, newContent);
		
		return LCSFileComparator.generateResult(lcpMatrix, oldContent, newContent);
	}
	
	private static int [][] createMatrix(final String [] oldContent, final String [] newContent) {
		
		final int m = oldContent.length;
		final int n = newContent.length;
		
		// last row and column are never used but
		// it was made to escape flooding loop with
		// silly constructions
		final int [] [] allocator = new int [m + 1] [n + 1];
		
		for(int i = m - 1; i >= 0; --i) {
			for(int j = n - 1; j >= 0; --j) {
				
				if(oldContent[i].equals(newContent[j]))
					allocator[i][j] = 1 + allocator[i + 1][j + 1]; 
				else
					allocator[i][j] = Math.max(allocator[i + 1][j], allocator[i][j + 1]);
			}
		}
		
		/*String matr = "";
		
		for(int i = 0; i < m; ++i) {
			for(int j = 0; j < n; ++j) {
				matr += allocator[i][j] + " ";
			}
			matr += '\n';
		}
		
		System.out.println(matr);*/
		
		return allocator;
	}
	
	private static CompareResult generateResult(int [][] matrix, final String [] oldContent, final String [] newContent) {
		
		final int m = oldContent.length;
		final int n = newContent.length;
		
		String [] cpyOld = Arrays.copyOf(oldContent, oldContent.length);
		String [] cpyNew = Arrays.copyOf(newContent, newContent.length);
		
		List<CmpRow> resultList = new ArrayList<>(oldContent.length + newContent.length - matrix[0][0]);
		
		int i = 0, j = 0;
		
		while(i < m && j < n) {
			
			if(cpyOld[i].equals(cpyNew[j])) {
				resultList.add(new CmpRow(cpyOld[i]));
				cpyOld[i] = cpyNew[j] = null;
				i++; j++;
			}
			else if(matrix[i + 1][j] >= matrix[i][j + 1]) i++;
			else j++;
		}
		
		for(i = 0; i < m; ++i) {
			if(cpyOld[i] != null) resultList.add(new CmpRow(i + 1, cpyOld[i], Type.DELETED));
		}
		
		for(j = 0; j < n; ++j) {
			if(cpyNew[j] != null) resultList.add(new CmpRow(j + 1, cpyNew[j], Type.ADDED));
		}
		
		return new CompareResult(resultList);
	}
	
	/*public static void main(String [] args) throws FileNotFoundException, IOException {
		
		
		String [] oldStr = FileUtils.read(new BufferedReader(new FileReader("D:/bitbucket repo/CVS Lite/src/resources/text1.txt"))).split("\n");//new String[] {"abbcdefg", "bcz","aaa", "kkkkkk", "w"};
	
		String [] newStr = FileUtils.read(new BufferedReader(new FileReader("D:/bitbucket repo/CVS Lite/src/resources/text2.txt"))).split("\n");//new String[] {"bcz","kkkkkk", "oop", "l"};
		
		long totalStart = System.currentTimeMillis(), totalStop = 0;
		for(int i = 0; i < 50; ++i) {
		long processStart = 0, processStop = 0;
		long lcsStart = 0, lcsStop = 0;
		
		processStart = System.currentTimeMillis();
		int [][] matrix = LCSFileComparator.process(oldStr, newStr);
		processStop = System.currentTimeMillis();
		
		lcsStart = System.currentTimeMillis();
		LCSFileComparator.lcsString(matrix, oldStr, newStr);
		lcsStop = System.currentTimeMillis();
		
		//System.out.println("Process " + (processStop - processStart));
		//System.out.println("Lcs " + (lcsStop - lcsStart));
		}
		totalStop = System.currentTimeMillis();
		System.out.println("Total " + (totalStop - totalStart));
	}*/

}
