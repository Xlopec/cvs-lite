package com.ua.max.oliynick.cvs.utils;

public class OptionsManager {
	
	private static GeneralPreferences generalPreferences;
	
	private OptionsManager() {}

	public static synchronized GeneralPreferences getGeneralOptions() {
		
		if(generalPreferences == null) {
			generalPreferences = new GeneralPreferences();
		}
		
		return generalPreferences;
	}
}
