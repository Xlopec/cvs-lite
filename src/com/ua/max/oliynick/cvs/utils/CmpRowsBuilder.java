package com.ua.max.oliynick.cvs.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javafx.scene.Parent;
import javafx.util.Builder;

import com.ua.max.oliynick.cvs.interfaces.ViewableRow;
import com.ua.max.oliynick.cvs.utils.CmpRow.Type;
import com.ua.max.oliynick.cvs.view.CmpViewableRowImp;

public class CmpRowsBuilder implements Builder<List<ViewableRow<Parent, CmpRow>>> {
	
	private boolean showUnchanged = true;
	private boolean showDeleted = true;
	private boolean showAdded = true;
	
	private boolean groupByIndx = false;
	private boolean groupByType = true;
	
	private final CompareResult rows;
	
	public CmpRowsBuilder(final CompareResult rows) {
		this.rows = rows;
	}
	
	public CmpRowsBuilder showUnchangedLines() {
		showUnchanged = true;
		return this;
	}
	
	public CmpRowsBuilder showDeletedLines() {
		showDeleted = true;
		return this;
	}
	
	public CmpRowsBuilder showAddedLines() {
		showAdded = true;
		return this;
	}
	
	public CmpRowsBuilder hideUnchangedLines() {
		showUnchanged = false;
		return this;
	}
	
	public CmpRowsBuilder hideDeletedLines() {
		showDeleted = false;
		return this;
	}
	
	public CmpRowsBuilder hideAddedLines() {
		showAdded = false;
		return this;
	}
	
	public CmpRowsBuilder groupByRowIndx() {
		groupByIndx = true;
		groupByType = false;
		return this;
	}
	
	public CmpRowsBuilder groupByType() {
		groupByIndx = false;
		groupByType = true;
		return this;
	}

	public boolean isShowUnchanged() {
		return showUnchanged;
	}

	public boolean isShowDeleted() {
		return showDeleted;
	}

	public boolean isShowAdded() {
		return showAdded;
	}
	
	public boolean isGroupByIndx() {
		return groupByIndx;
	}
	
	public boolean isGroupByType() {
		return groupByType;
	}

	public CompareResult getRows() {
		return rows;
	}

	@Override
	public List<ViewableRow<Parent, CmpRow>> build() {
		
		List<ViewableRow<Parent, CmpRow>> result = new ArrayList<>();
		List<CmpRow> rows = getRows().getComparedRows();
		boolean hideType = !isShowUnchanged() || !isShowDeleted() || !isShowAdded();
		
		if(hideType) {
			
			List<CmpRow> tmp = new ArrayList<CmpRow>(40);
			
			for(CmpRow tmpRow : rows) {
				if(isShowAdded() && tmpRow.getStatus() == Type.ADDED) {tmp.add(tmpRow); continue;}
				if(isShowUnchanged() && tmpRow.getStatus() == Type.UNCHANGED) {tmp.add(tmpRow); continue;}
				if(isShowDeleted() && tmpRow.getStatus() == Type.DELETED) {tmp.add(tmpRow); continue;}
			}
			
			rows.clear();
			Collections.sort(tmp, isGroupByType() ? CmpRow.BY_TYPE : CmpRow.BY_ROW_INDX);
			rows = tmp;
			
		} else {
			Collections.sort(rows, isGroupByType() ? CmpRow.BY_TYPE : CmpRow.BY_ROW_INDX);
		}
		
		rows.forEach(i -> result.add(new CmpViewableRowImp(i)));
		
		return result;
	}

}
