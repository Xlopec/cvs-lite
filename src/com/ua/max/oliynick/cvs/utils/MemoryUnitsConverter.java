package com.ua.max.oliynick.cvs.utils;

public class MemoryUnitsConverter {
	
	public static final long BINARY_PREFIX = 1024;
	public static final long SI_PREFIX = 1000;
	
	public static long toKilobytes(long bytes) {
		return bytes / BINARY_PREFIX;
	}

	public static long toMegabytes(long bytes) {
		return toKilobytes(bytes) / BINARY_PREFIX;
	}
	
	public static long toGigabytes(long bytes) {
		return toMegabytes(bytes) / BINARY_PREFIX;
	}
	
	public static long toTerabytes(long bytes) {
		return toGigabytes(bytes) / BINARY_PREFIX;
	}
	
	public static long fromKilobytes(long bytes) {
		return bytes * BINARY_PREFIX;
	}

	public static long fromMegabytes(long megabytes) {
		return megabytes * BINARY_PREFIX;
	}
	
	public static long fromGigabytes(long gigabytes) {
		return gigabytes * BINARY_PREFIX;
	}
	
	public static long fromTerabytes(long terabytes) {
		return terabytes * BINARY_PREFIX;
	}
	
}
