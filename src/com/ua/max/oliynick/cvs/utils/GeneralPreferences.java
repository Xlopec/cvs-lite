package com.ua.max.oliynick.cvs.utils;

import java.util.Locale;

import com.ua.max.oliynick.cvs.interfaces.Options;

public class GeneralPreferences implements Options {
	
	private Locale locale = Locale.ENGLISH;
	
	private String dateFormat = "dd.MM.yy mm:hh";
	
	public Locale getLocale() {
		return locale;
	}
	
	public void setLocale(final Locale locale) {
		this.locale = locale;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(final String dateFormat) {
		this.dateFormat = dateFormat;
	}

}
