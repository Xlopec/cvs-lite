package com.ua.max.oliynick.cvs.utils;

import java.util.Comparator;

public class Row {
	
	public static final Comparator<CmpRow> BY_ROW_INDX = new Comparator<CmpRow>() {
		
		@Override
		public int compare(CmpRow o1, CmpRow o2) {
			
			if (o1.getIndx() > o2.getIndx()) return +1;
			if(o1.getIndx() < o2.getIndx()) return -1;
			
			return 0;
		}
	};
	
	private String content;
	private int row;
	
	public Row(int row, final String content) {
		this.row = row;
		this.content = content;
	}
	
	public Row(final Row another) {
		this.content = another.content;
		this.row = another.row;
	}

	public String getContent() {
		return content;
	}

	public void setContent(final String content) {
		this.content = content;
	}

	public int getIndx() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

}
