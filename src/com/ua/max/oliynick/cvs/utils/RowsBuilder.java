package com.ua.max.oliynick.cvs.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.scene.Parent;
import javafx.util.Builder;

import com.ua.max.oliynick.cvs.interfaces.ViewableRow;
import com.ua.max.oliynick.cvs.view.ViewableRowImp;

public class RowsBuilder implements Builder<List<ViewableRow<Parent, Row>>> {
	
	private final String [] content;
	
	public RowsBuilder(final String [] content) {
		this.content = Arrays.copyOf(content, content.length);
	}

	@Override
	public List<ViewableRow<Parent, Row>> build() {
		
		List<ViewableRow<Parent, Row>> viewableRows = new ArrayList<>(content.length);
		
		for(int i = 0; i < content.length; ++i) {
			viewableRows.add(new ViewableRowImp(new Row(i + 1, content[i])));
		}
		
		return viewableRows;
	}

}
