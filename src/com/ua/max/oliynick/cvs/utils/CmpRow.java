package com.ua.max.oliynick.cvs.utils;

import java.util.Comparator;

public class CmpRow extends Row {
	
	public static enum Type {  UNCHANGED, ADDED, DELETED }
	
	public static final Comparator<CmpRow> BY_ROW_INDX = new Comparator<CmpRow>() {
		
		@Override
		public int compare(CmpRow o1, CmpRow o2) {
			
			if (o1.getIndx() > o2.getIndx()) return +1;
			if(o1.getIndx() < o2.getIndx()) return -1;
			
			return 0;
		}
	};
	
	public static final Comparator<CmpRow> BY_TYPE = new Comparator<CmpRow>() {
		
		@Override
		public int compare(CmpRow o1, CmpRow o2) {
			
			if (o1.getStatus().ordinal() < o2.getStatus().ordinal()) return +1;
			if(o1.getStatus().ordinal() > o2.getStatus().ordinal()) return -1;
			
			return 0;
		}
	};
	
	private Type status;
	
	public CmpRow(final String content) {
		this(-1, content, Type.UNCHANGED);
	}
	
	public CmpRow(int row, final String content, final Type status) {
		super(row, content);
		this.status = status;
	}
	
	public CmpRow(final CmpRow another) {
		super(another);
		this.status = another.status;
	}

	public Type getStatus() {
		return status;
	}

	public void setStatus(final Type status) {
		this.status = status;
	}

}
