package com.ua.max.oliynick.cvs.utils;

import java.util.ArrayList;
import java.util.List;

public class CompareResult {
	
	private final List<CmpRow> cmpRows;
	
	public CompareResult(final List<CmpRow> cmpRows) {
		
		if(cmpRows == null)
			throw new IllegalArgumentException("the first arg == null");
		
		this.cmpRows = new ArrayList<CmpRow>(cmpRows.size());
		
		cmpRows.forEach(i -> this.cmpRows.add(new CmpRow(i)));
	}
	
	public List<CmpRow> getComparedRows() {
		return cmpRows;
	}
	
	public List<String> getContent() {
		
		List<String> content = new ArrayList<String>(cmpRows.size());
		
		cmpRows.forEach(i -> content.add(i.getContent()));
		
		return content;
	}
	
}
