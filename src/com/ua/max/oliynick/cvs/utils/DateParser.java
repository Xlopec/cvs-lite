package com.ua.max.oliynick.cvs.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**This class allows to verify date.
 * The default date pattern is {@code DATE_PATTERN = "dd.MM.yyyy"}.
 * @see {@link SimpleDateFormat}*/
public class DateParser {

	/**Default date pattern {@value "dd.MM.yyyy"}*/
	public static final String DATE_PATTERN = "dd.MM.yyyy";
    
	/**Formats given milliseconds to given pattern.
	 * @param pattern - given pattern. If it is null, 
	 * {@code DATE_PATTERN = "dd.MM.yyyy"} will be used.
	 * @param millis - given time in milliseconds representation.
	 * @return String representation or null if can not be formatted.*/
    public static String formate(String pattern, long millis){
    	if(pattern == null)
    		pattern = DATE_PATTERN;
    	
    	SimpleDateFormat sdf = new SimpleDateFormat(pattern);
    	sdf.setLenient(false);
    	
    	return sdf.format(new Date(millis));
    }
    
    /**Parses given date by using given pattern. Only {@code DATE_PATTERN = "dd.MM.yyyy"}
     * can be used. The most strict parse method.
     * @param dateString - given date
     * @return Date object if parsed successfully or null
     * if problems caused.*/
    public static Date parse(String dateString){
	if(dateString == null) return null;
	
	if(!dateString.matches("\\d{2}[\\.-]\\d{2}[\\.-]\\d{4}")) return null;
	
    	SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
    	sdf.setLenient(false);
    	
    	try {
			return sdf.parse(dateString);
		} catch (ParseException e) {
			return null;
		}
    }
    
    /**Parses given date by using given pattern.
     * @param pattern - given pattern
     * @param dateString - given date
     * @return Date object if parsed successfully or null
     * if problems caused.*/
    public static Date parse(String pattern, String dateString){
    	
    	if(pattern == null)
    		pattern = DATE_PATTERN;
    	
    	SimpleDateFormat sdf = new SimpleDateFormat(pattern);
    	sdf.setLenient(false);
    	
    	try {
			return sdf.parse(dateString);
		} catch (ParseException e) {
			return null;
		}
    }
    
    /**Parses given date by using given pattern.
     * @param pattern - given pattern
     * @param dateString - given date
     * @param isLenient - to parse strictly or not. If this parameter is true
     * it will parse not strictly and, for example, 29 Feb 2001 will be accepted but it will
     * return 01 Jan 2001.
     * @return Date object if parsed successfully or null
     * if problems caused.*/
    public static Date parse(String pattern, String dateString, boolean isLenient){
    	
    	if(pattern == null)
    		pattern = DATE_PATTERN;
    	
    	SimpleDateFormat sdf = new SimpleDateFormat(pattern);
    	sdf.setLenient(isLenient);
    	
    	try {
			return sdf.parse(dateString);
		} catch (ParseException e) {
			return null;
		}
    }
    
    /**The most strict check. Only {@code DATE_PATTERN = "dd.MM.yyyy"}
     * can be used for it.
     * @param dateString - given date string*/
    public static boolean isValidDate(final String dateString) {
        return DateParser.parse(dateString) != null;
    }
    
    /**Checks DateString for correctness.
     * @param dateString - given date string
     * @param pattern - given pattern. Can be null, in this case default
     * pattern {@code DATE_PATTERN = "dd.MM.yyyy"} will be used.*/
    public static boolean isValidDate(final String pattern, final String dateString) {
        return DateParser.parse(pattern, dateString) != null;
    }
    
    /**Checks DateString for correctness.
     * @param dateString - given date string
     * @param pattern - given pattern. Can be null, in this case default
     * pattern {@code DATE_PATTERN = "dd.MM.yyyy"} will be used.
     * @param isLenient - to parse strictly or not. If this parameter is true
     * it will parse not strictly and, for example, 29 Feb 2001 will be accepted but it will
     * return 01 Jan 2001.*/
    public static boolean isValidDate(final String pattern, final String dateString, boolean isLenient) {
        return DateParser.parse(pattern, dateString, isLenient) != null;
    }
}
