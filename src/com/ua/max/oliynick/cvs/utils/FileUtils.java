package com.ua.max.oliynick.cvs.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FileUtils {
	
	private static String splitter = "\n";

	public FileUtils() {}
	
	public static void setSplitter(String split){
		splitter = split;
	}
	
	public static String getCurrentSplitter(){
		return splitter;
	}

	/**Returns true if was successfully written in file,
	 * false in another case;
	 * @throws IOException */
	public static boolean write(String directory, String text, boolean append) throws IOException{
		return write(new File(directory), text, append);
	}
	
	/**Returns true if was successfully written in file,
	 * false in another case;
	 * @throws IOException */
	public static boolean write(File file, String text, boolean append) throws IOException{
		if(text == null)
			throw new NullPointerException("text == null");
		
		BufferedWriter bw = null;
		
		try {
		bw = new BufferedWriter(new FileWriter(file, append));
		String words[] = text.split(splitter);
		
		for (String str : words) {
			bw.write(str);
			bw.newLine();
		}
			bw.close();
		} catch(IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**Returns true if file was successfully cleared,
	 * false in another case;*/
	public static boolean clearFile(String file){
		FileWriter fw = null;
		try{
		fw = new FileWriter(file);
		}catch(IOException e){
			return false;
		} finally {
			if(fw != null)
				try {
					fw.close();
				} catch (IOException e) { return false; }
		}
		return true;
	}
	
	/**Returns true if file was successfully cleared,
	 * false in another case;*/
	public static boolean clearFile(File file){
		FileWriter fw = null;
		try{
		fw = new FileWriter(file);
		}catch(IOException e){
			return false;
		} finally {
			if(fw != null)
				try {
					fw.close();
				} catch (IOException e) { return false; }
		}
		return true;
	}
	
	public static File createFile(final String dir) {
		FileWriter writer = null;
		File file = new File(dir);
		try {
			writer = new FileWriter(file);
			
			return file;
		} catch (IOException e1) {
			return null;
		} finally {
			if(writer != null)
				try {
					writer.close();
				} catch (IOException e) { return null; }
		}
	}

	public static String read(BufferedReader br) throws IOException {
		StringBuilder result = new StringBuilder();

		String s = null;

		while ((s = br.readLine()) != null) {
			s += '\n';
			result.append(s);
		}

		return result.toString();
	}
	
	// convert InputStream to String
	public static String getStringFromInputStream(InputStream is) {

			BufferedReader br = null;
			StringBuilder sb = new StringBuilder();

			String line;
			try {

				br = new BufferedReader(new InputStreamReader(is));
				while ((line = br.readLine()) != null) {
					sb.append(line);
				}

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			return sb.toString();

		}

}
