package com.ua.max.oliynick.cvs.launcher;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Callback;

import com.ua.max.oliynick.cvs.controller.CmpFileManagerControllerImp;
import com.ua.max.oliynick.cvs.controller.FileManagerControllerImp;
import com.ua.max.oliynick.cvs.factories.DefaultFileViewFactory;
import com.ua.max.oliynick.cvs.factories.FileComparatorManager;
import com.ua.max.oliynick.cvs.interfaces.ICmpContentManagerModel;
import com.ua.max.oliynick.cvs.interfaces.ICmpFileContentController;
import com.ua.max.oliynick.cvs.interfaces.ICmpFileContentModel;
import com.ua.max.oliynick.cvs.interfaces.ICmpFileManagerController;
import com.ua.max.oliynick.cvs.interfaces.IContentManagerModel;
import com.ua.max.oliynick.cvs.interfaces.IController;
import com.ua.max.oliynick.cvs.interfaces.IFileContentController;
import com.ua.max.oliynick.cvs.interfaces.FileContentModel;
import com.ua.max.oliynick.cvs.interfaces.IFileManagerController;
import com.ua.max.oliynick.cvs.interfaces.FileViewFactory;
import com.ua.max.oliynick.cvs.interfaces.IView;
import com.ua.max.oliynick.cvs.interfaces.View;
import com.ua.max.oliynick.cvs.model.CmpFileManagerModel;
import com.ua.max.oliynick.cvs.model.FileManagerModel;
import com.ua.max.oliynick.cvs.utils.CmpRowsBuilder;
import com.ua.max.oliynick.cvs.utils.FileUtils;
import com.ua.max.oliynick.cvs.utils.RowsBuilder;

public class Launcher extends Application {
	
	private static View<IContentManagerModel, IFileManagerController> fileContentView;
	private static View<ICmpContentManagerModel, ICmpFileManagerController> cmpFileContentView;
	private static FileViewFactory factory = new DefaultFileViewFactory();
	
	private void buildFileTree(TreeItem<String> tree, File dir) {
		
		if(dir.isDirectory()) {
			
			System.out.println("\t\tDirectory " + dir.getName());
			
			String [] list = dir.list();
			
			for(int i = 0; i < list.length; ++i) {
				
				File tmp = new File(dir.getAbsolutePath()+"/"+list[i]);
				if(tmp.isDirectory()) {
					TreeItem<String> tr = new TreeItem<String> (tmp.getName());
					tree.getChildren().add(tr);
					buildFileTree(tr, tmp);
				} else {
					tree.getChildren().add(new TreeItem<String> (tmp.getName()));
					System.out.println(list[i]);
				}
			}
		}
		
	}
	
	@Override
	public void start(final Stage primaryStage) throws FileNotFoundException, IOException {
		
		Platform.setImplicitExit(true);
		Locale.setDefault(Locale.ENGLISH);
		
		File file1, file2, dir;
		
		dir = new File("D:/bitbucket repo/CVS Lite/src");
		file1 = new File("D:/bitbucket repo/CVS Lite/src/resources/text1.txt");
		file2 = new File("D:/bitbucket repo/CVS Lite/src/resources/text2.txt");
		
		/*String [] oldStr = FileUtils.read(new BufferedReader(new FileReader(file1))).split("\n");
		String [] content = FileUtils.read(new BufferedReader(new FileReader(file2))).split("\n");
		
		String [] someTxt = new String [] {
		"View v1 = factory.createFilesViewContent(simpleBuilder);",
		"View v2 = factory.createFilesViewContent(cmpBuilder);",
		"View v3 = factory.createFilesViewContent(cmpBuilder);"
		};
		
		RowsBuilder rBuilder = new RowsBuilder(content);
		CmpRowsBuilder cmpBuilder = new CmpRowsBuilder(FileComparatorManager.getDefaultComparator().compare(oldStr, content));
		
		IView<ICmpFileContentModel, ICmpFileContentController> v1 = factory.createCmpContentView(cmpBuilder.build(), file1);
		IView<FileContentModel, IFileContentController> v2 = factory.createContentView(rBuilder.build(), file2);
		v2.setTitle("file 2");
		
		IContentManagerModel model = fileContentView.getModel();
		model.putChildren(v2);
		model.putChildren(factory.createContentView(rBuilder.build(), file2));
		model.putChildren(factory.createContentView(rBuilder.build(), file2));
		
		cmpFileContentView.getModel().putChildren(v1);
		
		TabPane root = new TabPane(new Tab("file view", fileContentView.getRoot()), new Tab("cmp view", cmpFileContentView.getRoot()));
		root.setSide(Side.BOTTOM);*/
		
		/*TreeItem<String> root = new TreeItem<> ("Messages");
		
		TreeItem<String> inboxItem = new TreeItem<String> ("Inbox");
        inboxItem.setExpanded(true);
        for (int i = 1; i < 6; i++) {
            TreeItem<String> item = new TreeItem<String> ("Message" + i);            
            inboxItem.getChildren().add(item);
        } 
        
        TreeItem<String> outboxItem = new TreeItem<String> ("Outbox");
        for (int i = 10; i < 20; i++) {
            TreeItem<String> item = new TreeItem<String> ("Message" + i);            
            outboxItem.getChildren().add(item);
        } 
        
        root.getChildren().addAll(inboxItem, outboxItem);*/
		
        
        
        TreeItem<String> root = new TreeItem<String>(dir.toString());
        
        buildFileTree(root, dir);
        
        TreeView<String> tree = new TreeView<String> (root); 
		
		StackPane sp = new StackPane(tree);
		
		Scene scene = new Scene(sp);
		primaryStage.setScene(scene);
		primaryStage.setTitle("test");
		primaryStage.show();
	
	}
	
	@Override
	public void init() throws Exception {
		super.init();
		
		loadFileContentView();
		loadCmpFileContentView();
	}
	
	private static void loadFileContentView() {
		FXMLLoader loader = new FXMLLoader(DefaultFileViewFactory.class.getClassLoader().getResource("resources/fxml/FileContentHolder.fxml"));
		loader.setControllerFactory(new Callback<Class<?>, Object>() {
			
			@Override
			public Object call(Class<?> param) {
				
				IFileManagerController c = null;
				FileManagerModel m = new FileManagerModel();
				
				c = new FileManagerControllerImp();
				m.setController(c);
				c.setModel(m);
				
				return c;
			}
		});
		
		try {
			Node node = loader.load();
			IFileManagerController controller = loader.getController();
			
			fileContentView = new View<IContentManagerModel, IFileManagerController>("file preview", null, node, controller, controller.getModel());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void loadCmpFileContentView() {
		FXMLLoader loader = new FXMLLoader(DefaultFileViewFactory.class.getClassLoader().getResource("resources/fxml/FileContentHolder.fxml"));
		loader.setControllerFactory(new Callback<Class<?>, Object>() {
			
			@Override
			public Object call(Class<?> param) {
				
				ICmpFileManagerController c = null;
				CmpFileManagerModel m = new CmpFileManagerModel();
				
				c = new CmpFileManagerControllerImp();
				m.setController(c);
				c.setModel(m);
				
				return c;
			}
		});
		
		try {
			Node node = loader.load();
			ICmpFileManagerController controller = loader.getController();
			
			cmpFileContentView = new View<ICmpContentManagerModel, ICmpFileManagerController>("cmp preview", null, node, controller, controller.getModel());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
