package com.ua.max.oliynick.cvs.interfaces;

import java.io.File;
import java.util.List;

import javafx.scene.Parent;

import com.ua.max.oliynick.cvs.utils.CmpRow;
import com.ua.max.oliynick.cvs.utils.Row;

public abstract class FileViewFactory {
	
	public abstract IView<FileContentModel, IFileContentController> createContentView
	(final List<ViewableRow<Parent, Row>> data, File file);
	
	public abstract IView<ICmpFileContentModel, ICmpFileContentController> createCmpContentView
	(final List<ViewableRow<Parent, CmpRow>> data, File file);
}
