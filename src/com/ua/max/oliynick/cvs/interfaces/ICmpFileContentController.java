package com.ua.max.oliynick.cvs.interfaces;

public abstract class ICmpFileContentController implements IController {
	
	private ICmpFileContentModel model;
	
	public ICmpFileContentModel getModel() {
		return model;
	}
	
	public void setModel(final ICmpFileContentModel model) {
		this.model = model;
	}
	
	public abstract void onHideAddedRows();
	
	public abstract void onHideUnchangedRows();
	
	public abstract void onHideRemovedRows();

	public abstract void onShowAddedRows();
	
	public abstract void onShowUnchangedRows();
	
	public abstract void onShowRemovedRows();
	
	public abstract void onHideShowDetailsMenu();
	
}
