package com.ua.max.oliynick.cvs.interfaces;

import java.util.List;

import javafx.scene.Parent;

import com.ua.max.oliynick.cvs.utils.CmpRow;

public interface ICmpFileContentModel extends IModel {

	public List<String> getRawContent();
	
	public List<ViewableRow<Parent, CmpRow>> getViewableContent();
	
	public int getRows();
	
	public long getFileSize();
	
	public String getFormatedFileSize();

	public String getLastModified();
	
	public String getFilename();
	
	public int getDeletedRows();
	
	public int getAddedRows();
	
	public int getUnchangedRows();
	
}
