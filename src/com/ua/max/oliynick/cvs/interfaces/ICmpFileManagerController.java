package com.ua.max.oliynick.cvs.interfaces;

import com.ua.max.oliynick.cvs.view.TabbedView;

public abstract class ICmpFileManagerController implements IController {
	
	private ICmpContentManagerModel model;

	public ICmpContentManagerModel getModel() {
		return model;
	}

	public void setModel(ICmpContentManagerModel model) {
		this.model = model;
	}

	public abstract boolean open(int indx);
	
	public abstract boolean open(final TabbedView <ICmpFileContentModel, ICmpFileContentController> v);

	public abstract boolean close(int indx);
	
	public abstract boolean close(final TabbedView <ICmpFileContentModel, ICmpFileContentController> v);
	
	public abstract void closeAll();
	
	public abstract void put(final TabbedView <ICmpFileContentModel, ICmpFileContentController> v);
}