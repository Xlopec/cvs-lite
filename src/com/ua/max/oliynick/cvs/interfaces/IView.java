package com.ua.max.oliynick.cvs.interfaces;

import javafx.scene.Node;

public interface IView <M extends IModel, Cntrl extends IController> {
	
	public void setTitle(final String title);
	
	public String getTitle();
	
	public String getViewId();
	
	public M getModel();

	public Node getRoot();

	public Cntrl getController();

}
