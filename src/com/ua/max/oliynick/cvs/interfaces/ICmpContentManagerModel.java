package com.ua.max.oliynick.cvs.interfaces;

import java.util.Collection;

import com.ua.max.oliynick.cvs.utils.GeneralPreferences;

public interface ICmpContentManagerModel extends IAdjustable<GeneralPreferences>, IModel {
	
	public boolean open(int indx);
	
	public boolean open(final IView<ICmpFileContentModel, ICmpFileContentController> v);

	public boolean close(int indx);
	
	public boolean close(final IView<ICmpFileContentModel, ICmpFileContentController> v);
	
	public void closeAll();
	
	public void restoreRecent();
	
	public void restoreAll();
	
	public Collection<IView<ICmpFileContentModel, ICmpFileContentController>> getContent();
	
	public void putChildren(final IView<ICmpFileContentModel, ICmpFileContentController> v);

}