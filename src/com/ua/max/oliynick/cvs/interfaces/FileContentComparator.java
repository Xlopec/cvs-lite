package com.ua.max.oliynick.cvs.interfaces;

import com.ua.max.oliynick.cvs.utils.CompareResult;

public interface FileContentComparator {
	
	public CompareResult compare(final String [] oldContent, final String [] newContent);

}
