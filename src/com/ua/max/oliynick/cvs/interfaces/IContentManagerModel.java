package com.ua.max.oliynick.cvs.interfaces;

import java.util.Collection;

import com.ua.max.oliynick.cvs.utils.GeneralPreferences;

public interface IContentManagerModel extends IAdjustable<GeneralPreferences>, IModel {
	
	public boolean open(int indx);
	
	public boolean open(final IView<FileContentModel, IFileContentController> v);

	public boolean close(int indx);
	
	public boolean close(final IView<FileContentModel, IFileContentController> v);
	
	public void closeAll();
	
	public void restoreRecent();
	
	public void restoreAll();
	
	public Collection<IView<FileContentModel, IFileContentController>> getContent();
	
	public void putChildren(final IView<FileContentModel, IFileContentController> v);

}
