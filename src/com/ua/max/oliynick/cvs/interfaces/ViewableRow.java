package com.ua.max.oliynick.cvs.interfaces;

import javafx.scene.Parent;

import com.ua.max.oliynick.cvs.utils.Row;

public abstract class ViewableRow <T extends Parent, R extends Row> {
	
	private final R row;
	private T root;
	
	public ViewableRow(final R row) {
		this.row = row;
	}
	
	public R getRow() {
		return row;
	}
	
	protected abstract T buildView();
	
	public T getRoot() {
		
		if(root == null) root = buildView();
		
		return root;
	}
}
