package com.ua.max.oliynick.cvs.interfaces;

public abstract class IFileContentController implements IController {
	
	private FileContentModel model;
	
	public void setModel(final FileContentModel model) {
		this.model = model;
	}
	
	public FileContentModel getModel() {
		return model;
	}

	public abstract void onDropAnotherTab();

	public abstract void onHideShowDetailsMenu();
}
