package com.ua.max.oliynick.cvs.interfaces;

import com.ua.max.oliynick.cvs.view.TabbedView;

public abstract class IFileManagerController implements IController {
	
	private IContentManagerModel model;

	public IContentManagerModel getModel() {
		return model;
	}

	public void setModel(IContentManagerModel model) {
		this.model = model;
	}

	public abstract boolean open(int indx);
	
	public abstract boolean open(final TabbedView <FileContentModel, IFileContentController> v);

	public abstract boolean close(int indx);
	
	public abstract boolean close(final TabbedView <FileContentModel, IFileContentController> v);
	
	public abstract void closeAll();
	
	public abstract void put(final TabbedView <FileContentModel, IFileContentController> v);
}
