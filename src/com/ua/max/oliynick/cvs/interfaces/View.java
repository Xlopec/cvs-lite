package com.ua.max.oliynick.cvs.interfaces;

import javafx.scene.Node;

public class View <M extends IModel, Cntrl extends IController> implements IView <M, Cntrl>{
	
	private String title;
	
	private final String id;
	
	private final Cntrl controller;
	
	private final M model;
	
	private final Node root;
	
	public View(final Node root, final Cntrl c, final M m) {
		this(null, null, root, c, m);
	}
	
	public View(final String title, final Node root, final Cntrl c, final M m) {
		this(null, title, root, c, m);
	}
	
	public View(final String title, final String id, final Node root, final Cntrl c, final M m) {
		this.title = title;
		this.id = null;
		this.controller = c;
		this.model = m;
		this.root = root;
	}
	
	public void setTitle(final String title) {
		this.title = title;
	}
	
	@Override
	public String getTitle() {
		return title;
	}
	
	@Override
	public String getViewId() {
		return id;
	}
	
	@Override
	public M getModel() {
		return model;
	}

	@Override
	public Node getRoot() {
		return root;
	}

	@Override
	public Cntrl getController() {
		return controller;
	}
	
}
