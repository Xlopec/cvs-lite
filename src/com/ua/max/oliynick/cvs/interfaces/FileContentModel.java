package com.ua.max.oliynick.cvs.interfaces;

import java.util.List;

import javafx.scene.Parent;

import com.ua.max.oliynick.cvs.utils.Row;

public interface FileContentModel extends IModel {
	
	public List<String> getRawContent();
	
	public List<ViewableRow<Parent, Row>> getViewableContent();
	
	public int getRows();
	
	public long getFileSize();
	
	public String getFormatedFileSize();

	public String getLastModified();
	
	public String getFilename();
	
}
