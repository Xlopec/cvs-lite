package com.ua.max.oliynick.cvs.interfaces;

public interface IAdjustable <T extends Options> {
	
	public void adjust(final T t);

}
