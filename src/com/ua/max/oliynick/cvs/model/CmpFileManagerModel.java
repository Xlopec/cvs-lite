package com.ua.max.oliynick.cvs.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ua.max.oliynick.cvs.interfaces.ICmpContentManagerModel;
import com.ua.max.oliynick.cvs.interfaces.ICmpFileContentController;
import com.ua.max.oliynick.cvs.interfaces.ICmpFileContentModel;
import com.ua.max.oliynick.cvs.interfaces.ICmpFileManagerController;
import com.ua.max.oliynick.cvs.interfaces.IController;
import com.ua.max.oliynick.cvs.interfaces.IModel;
import com.ua.max.oliynick.cvs.interfaces.IView;
import com.ua.max.oliynick.cvs.utils.GeneralPreferences;
import com.ua.max.oliynick.cvs.view.TabbedView;

public class CmpFileManagerModel implements ICmpContentManagerModel {
	
	private Map<Integer, TabbedView<? extends IModel, ? extends IController>> children;
	private int lastId = 1;
	
	private ICmpFileManagerController controller;
	
	public CmpFileManagerModel() {
		children = new HashMap<>();
	}
	
	public CmpFileManagerModel(final List<TabbedView<ICmpFileContentModel, ICmpFileContentController>> initialVals) {
		children = new HashMap<>(initialVals.size());
		initialVals.forEach(i -> putChildren(i));
	}
	
	public void setController(ICmpFileManagerController controller) {
		this.controller = controller;
	}
	
	public ICmpFileManagerController getController() {
		return controller;
	}
	
	@Override
	public void adjust(GeneralPreferences t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean open(int indx) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean open(IView<ICmpFileContentModel, ICmpFileContentController> v) {
		
		if(children.values().contains(v)) {
			// choose by controller
		} else {
			putChildren(v);
		}
		
		return false;
	}

	@Override
	public boolean close(int indx) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean close(IView<ICmpFileContentModel, ICmpFileContentController> v) {
		return children.values().remove(v);
	}

	@Override
	public void closeAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void restoreRecent() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void restoreAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Collection<IView<ICmpFileContentModel, ICmpFileContentController>> getContent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void putChildren(final IView<ICmpFileContentModel, ICmpFileContentController> i) {
		
		if(controller == null)
			throw new IllegalStateException("controller wasn't set");
		
		if(i == null)
			throw new IllegalArgumentException("parameter argument == null");
		
		lastId++;
		TabbedView<ICmpFileContentModel, ICmpFileContentController> tv = CmpFileManagerModel.fromView(i, lastId);
		children.put(lastId, tv);
		controller.put(tv);
	}
	
	private static TabbedView<ICmpFileContentModel, ICmpFileContentController> fromView(IView<ICmpFileContentModel, ICmpFileContentController> v, int id) {
		return new TabbedView<>(v.getTitle(), String.valueOf(id), v.getRoot(), v.getController(), v.getModel());
	}
	
}
