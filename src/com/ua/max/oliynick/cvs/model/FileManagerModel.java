package com.ua.max.oliynick.cvs.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ua.max.oliynick.cvs.interfaces.IContentManagerModel;
import com.ua.max.oliynick.cvs.interfaces.IController;
import com.ua.max.oliynick.cvs.interfaces.IFileContentController;
import com.ua.max.oliynick.cvs.interfaces.FileContentModel;
import com.ua.max.oliynick.cvs.interfaces.IFileManagerController;
import com.ua.max.oliynick.cvs.interfaces.IView;
import com.ua.max.oliynick.cvs.interfaces.IModel;
import com.ua.max.oliynick.cvs.utils.GeneralPreferences;
import com.ua.max.oliynick.cvs.view.TabbedView;

public class FileManagerModel implements IContentManagerModel {
	
	private Map<Integer, TabbedView<? extends IModel, ? extends IController>> children;
	private int lastId = 1;
	
	private IFileManagerController controller;
	
	public FileManagerModel() {
		children = new HashMap<>();
	}
	
	public FileManagerModel(final List<TabbedView<FileContentModel, IFileContentController>> initialVals) {
		children = new HashMap<>(initialVals.size());
		initialVals.forEach(i -> putChildren(i));
	}
	
	public void setController(IFileManagerController controller) {
		this.controller = controller;
	}
	
	public IFileManagerController getController() {
		return controller;
	}
	
	@Override
	public void adjust(GeneralPreferences t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean open(int indx) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean open(IView<FileContentModel, IFileContentController> v) {
		
		if(children.values().contains(v)) {
			// choose by controller
		} else {
			putChildren(v);
		}
		
		return false;
	}

	@Override
	public boolean close(int indx) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean close(IView<FileContentModel, IFileContentController> v) {
		return children.values().remove(v);
	}

	@Override
	public void closeAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void restoreRecent() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void restoreAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Collection<IView<FileContentModel, IFileContentController>> getContent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void putChildren(final IView<FileContentModel, IFileContentController> v) {
		
		if(controller == null)
			throw new IllegalStateException("controller wasn't set");
		
		if(v == null)
			throw new IllegalArgumentException("parameter argument == null");
		
		lastId++;
		TabbedView<FileContentModel, IFileContentController> tv = FileManagerModel.fromView(v, lastId);
		children.put(lastId, tv);
		controller.put(tv);
	}
	
	private static TabbedView<FileContentModel, IFileContentController> fromView(IView<FileContentModel, IFileContentController> v, int id) {
		return new TabbedView<>(v.getTitle(), String.valueOf(id), v.getRoot(), v.getController(), v.getModel());
	}
	
}
