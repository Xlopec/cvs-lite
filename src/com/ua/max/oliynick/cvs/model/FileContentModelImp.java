package com.ua.max.oliynick.cvs.model;

import java.io.File;
import java.lang.management.MemoryUsage;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.Parent;

import com.ua.max.oliynick.cvs.interfaces.FileContentModel;
import com.ua.max.oliynick.cvs.interfaces.ViewableRow;
import com.ua.max.oliynick.cvs.utils.DateParser;
import com.ua.max.oliynick.cvs.utils.MemoryUnitsConverter;
import com.ua.max.oliynick.cvs.utils.OptionsManager;
import com.ua.max.oliynick.cvs.utils.Row;

public class FileContentModelImp implements FileContentModel {
	
	private List<ViewableRow<Parent, Row>> content;
	
	private final File file;
	
	public FileContentModelImp(final List<ViewableRow<Parent, Row>> initContent, File file) {
		
		if(initContent == null)
			throw new IllegalArgumentException("initial content == null");
		
		if(file == null)
			throw new IllegalArgumentException("target file == null");
		
		this.content = new ArrayList<>(initContent.size());
		this.content.addAll(initContent);
		
		this.file = file;
	}
	
	@Override
	public List<ViewableRow<Parent, Row>> getViewableContent() {
		return content;
	}

	@Override
	public List<String> getRawContent() {
		List<String> rawContent = new ArrayList<>(content.size());
		content.forEach(i -> rawContent.add(i.getRow().getContent()));
		
		return rawContent;
	}

	@Override
	public int getRows() {
		return content.size();
	}

	@Override
	public long getFileSize() {
		return file.length();
	}

	@Override
	public String getLastModified() {
		return DateParser.formate(OptionsManager.getGeneralOptions().getDateFormat(), file.lastModified());
	}

	@Override
	public String getFormatedFileSize() {
		
		long prefix = MemoryUnitsConverter.SI_PREFIX;
		long fileSize = getFileSize();
		
		String [] prefixes = new String [] {
			"bytes", "Kb", "Mb", "Gb", "Tb"
		};
		
		for(int i = 0; i < prefixes.length; ++i) {
			if(fileSize <= prefix) {
				return String.valueOf(fileSize) + ' ' + prefixes[i];
			} else {
				fileSize /= MemoryUnitsConverter.SI_PREFIX;
				prefix *= MemoryUnitsConverter.SI_PREFIX;
			}
		}
		
		return String.valueOf(fileSize) + ' ' + prefixes[4];
	}

	@Override
	public String getFilename() {
		return file.getName();
	}

}
