package com.ua.max.oliynick.cvs.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javafx.scene.Parent;

import com.ua.max.oliynick.cvs.interfaces.ICmpFileContentModel;
import com.ua.max.oliynick.cvs.interfaces.ViewableRow;
import com.ua.max.oliynick.cvs.utils.CmpRow;
import com.ua.max.oliynick.cvs.utils.CmpRow.Type;
import com.ua.max.oliynick.cvs.utils.DateParser;
import com.ua.max.oliynick.cvs.utils.MemoryUnitsConverter;
import com.ua.max.oliynick.cvs.utils.OptionsManager;

public class CmpFileContentModelImp implements ICmpFileContentModel {
	
	private List<ViewableRow<Parent, CmpRow>> content;
	
	private final File file;
	
	private int deletedRows = 0;
	private int addedRows = 0;
	private int unchangedRows = 0;
	
	public CmpFileContentModelImp(final List<ViewableRow<Parent, CmpRow>> initContent, final File file) {
		this.content = new ArrayList<>(initContent.size());
		this.content.addAll(initContent);
		this.file = file;
		
		deletedRows = content.stream()
				.filter(i -> i.getRow().getStatus() == Type.DELETED)
				.collect(Collectors.toList()).size();
		
		addedRows = content.stream()
				.filter(i -> i.getRow().getStatus() == Type.ADDED)
				.collect(Collectors.toList()).size();
		
		unchangedRows = initContent.size() - deletedRows - addedRows;
	}
	
	public List<ViewableRow<Parent, CmpRow>> getViewableContent() {
		return content;
	}
	
	@Override
	public List<String> getRawContent() {
		List<String> rawContent = new ArrayList<>(content.size());
		content.forEach(i -> rawContent.add(i.getRow().getContent()));
		
		return rawContent;
	}

	@Override
	public int getRows() {
		return content.size();
	}

	@Override
	public long getFileSize() {
		return file.length();
	}

	@Override
	public String getLastModified() {
		return DateParser.formate(OptionsManager.getGeneralOptions().getDateFormat(), file.lastModified());
	}

	@Override
	public String getFormatedFileSize() {
		
		long prefix = MemoryUnitsConverter.SI_PREFIX;
		long fileSize = getFileSize();
		
		String [] prefixes = new String [] {
			"bytes", "Kb", "Mb", "Gb", "Tb"
		};
		
		for(int i = 0; i < prefixes.length; ++i) {
			if(fileSize <= prefix) {
				return String.valueOf(fileSize) + ' ' + prefixes[i];
			} else {
				fileSize /= MemoryUnitsConverter.SI_PREFIX;
				prefix *= MemoryUnitsConverter.SI_PREFIX;
			}
		}
		
		return String.valueOf(fileSize) + ' ' + prefixes[4];
	}

	@Override
	public int getDeletedRows() {
		return deletedRows;
	}

	@Override
	public int getAddedRows() {
		return addedRows;
	}

	@Override
	public int getUnchangedRows() {
		return unchangedRows;
	}

	@Override
	public String getFilename() {
		return file.getName();
	}

}
