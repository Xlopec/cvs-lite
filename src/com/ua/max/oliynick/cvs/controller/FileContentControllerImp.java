package com.ua.max.oliynick.cvs.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import com.ua.max.oliynick.cvs.interfaces.IFileContentController;
import com.ua.max.oliynick.cvs.interfaces.FileContentModel;
import com.ua.max.oliynick.cvs.interfaces.ViewableRow;
import com.ua.max.oliynick.cvs.utils.Row;

public class FileContentControllerImp extends IFileContentController implements Initializable {
	
	@FXML private ListView<Parent> contentListView;
	
	@FXML private Label rowsNumLabel;
	@FXML private Label fileSizeLabel;
	@FXML private Label lastModifiedLabel;
	
	@FXML private VBox detailsRootHBox;
	@FXML private HBox detailsHBox;
	
	@FXML private Button hideShowButton;
	
	public FileContentControllerImp() {
		super();
	}
	
	public FileContentControllerImp(final FileContentModel model) {
		super();
		super.setModel(model);
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		FileContentModel model = getModel();
		
		rowsNumLabel.setText(resources.getString("rows").concat(' ' + String.valueOf(model.getRows())));
		fileSizeLabel.setText(resources.getString("size").concat(' ' + model.getFormatedFileSize()));
		lastModifiedLabel.setText(resources.getString("last_mod").concat(' ' + model.getLastModified()));
		
		for(final ViewableRow<Parent, Row> viewableRow : getModel().getViewableContent()) {
		    contentListView.getItems().add(viewableRow.getRoot());
		}
		
		StringBuilder statusText = new StringBuilder();
		statusText.append(resources.getString("filename")).append(' ').append(model.getFilename());
		statusText.append('\n').append(resources.getString("rows")).append(' ').append(String.valueOf(model.getRows()));
		statusText.append('\n').append(resources.getString("size")).append(' ').append(model.getFormatedFileSize());
		statusText.append('\n').append(resources.getString("last_mod")).append(' ').append(model.getLastModified());
		
		contentListView.setTooltip(new Tooltip(statusText.toString()));
		
	}

	@Override
	public void onDropAnotherTab() {
		// TODO Auto-generated method stub
		
	}

	@FXML
	@Override
	public void onHideShowDetailsMenu() {
		
		boolean hidden = detailsRootHBox.getChildren().remove(detailsHBox);
		
		if(hidden) {
			hideShowButton.getStyleClass().remove("hide-btn");
			hideShowButton.getStyleClass().add("show-btn");
		} else {
			detailsRootHBox.getChildren().add(detailsHBox);
			hideShowButton.getStyleClass().remove("show-btn");
			hideShowButton.getStyleClass().add("hide-btn");
		}
	}

}
