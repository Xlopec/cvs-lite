package com.ua.max.oliynick.cvs.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import com.ua.max.oliynick.cvs.interfaces.ICmpFileContentController;
import com.ua.max.oliynick.cvs.interfaces.ICmpFileContentModel;
import com.ua.max.oliynick.cvs.interfaces.ViewableRow;
import com.ua.max.oliynick.cvs.utils.CmpRow;

public class CmpFileContentControllerImp extends ICmpFileContentController implements Initializable {
	
	@FXML private ListView<Parent> contentListView;
	
	@FXML private Label rowsNumLabel;
	@FXML private Label fileSizeLabel;
	@FXML private Label lastModifiedLabel;
	
	@FXML private VBox detailsRootHBox;
	@FXML private HBox detailsHBox;
	
	@FXML private Button hideShowButton;
	
	public CmpFileContentControllerImp() {
		super();
	}
	
	public CmpFileContentControllerImp(final ICmpFileContentModel model) {
		super();
		super.setModel(model);
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		ICmpFileContentModel model = getModel();
		
		rowsNumLabel.setText(resources.getString("rows").concat(' ' + String.valueOf(model.getRows())));
		fileSizeLabel.setText(resources.getString("size").concat(' ' + model.getFormatedFileSize()));
		lastModifiedLabel.setText(resources.getString("last_mod").concat(' ' + model.getLastModified()));
		
		for(final ViewableRow<Parent, CmpRow> viewableRow : model.getViewableContent()) {
			contentListView.getItems().add(viewableRow.getRoot());
		}
		
		StringBuilder statusText = new StringBuilder();
		statusText.append(resources.getString("filename")).append(' ').append(model.getFilename());
		statusText.append('\n').append(resources.getString("added_rows")).append(' ').append(model.getAddedRows());
		statusText.append('\n').append(resources.getString("deleted_rows")).append(' ').append(model.getDeletedRows());
		statusText.append('\n').append(resources.getString("unchanged_rows")).append(' ').append(model.getUnchangedRows());
		statusText.append('\n').append(resources.getString("rows")).append(' ').append(String.valueOf(model.getRows()));
		statusText.append('\n').append(resources.getString("size")).append(' ').append(model.getFormatedFileSize());
		statusText.append('\n').append(resources.getString("last_mod")).append(' ').append(model.getLastModified());
		
		contentListView.setTooltip(new Tooltip(statusText.toString()));
		
	}

	@Override
	public void onHideAddedRows() {
		
	}

	@Override
	public void onHideUnchangedRows() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onHideRemovedRows() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onShowAddedRows() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onShowUnchangedRows() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onShowRemovedRows() {
		// TODO Auto-generated method stub
		
	}

	@FXML
	@Override
	public void onHideShowDetailsMenu() {
		
		boolean hidden = detailsRootHBox.getChildren().remove(detailsHBox);
		
		if(hidden) {
			hideShowButton.getStyleClass().remove("hide-btn");
			hideShowButton.getStyleClass().add("show-btn");
		} else {
			detailsRootHBox.getChildren().add(detailsHBox);
			hideShowButton.getStyleClass().remove("show-btn");
			hideShowButton.getStyleClass().add("hide-btn");
		}
	}

}
