package com.ua.max.oliynick.cvs.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TabPane;

import com.ua.max.oliynick.cvs.interfaces.IContentManagerModel;
import com.ua.max.oliynick.cvs.interfaces.IFileContentController;
import com.ua.max.oliynick.cvs.interfaces.FileContentModel;
import com.ua.max.oliynick.cvs.interfaces.IFileManagerController;
import com.ua.max.oliynick.cvs.view.TabbedView;

public class FileManagerControllerImp extends IFileManagerController implements Initializable {

	@FXML private TabPane tabPane;
	
	public FileManagerControllerImp() {
		super();
	}
	
	public FileManagerControllerImp(IContentManagerModel model) {
		super();
		super.setModel(model);
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		/*for(TabbedView<FileContentModel, FileContentController> view : getModel().getContent()) {
			tabPane.getTabs().add(new Tab(view.getTitle(), view.getRoot()));
		}*/
		
	}

	@Override
	public boolean open(int indx) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean open(TabbedView<FileContentModel, IFileContentController> v) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean close(int indx) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean close(TabbedView<FileContentModel, IFileContentController> v) {
		return getModel().close(v);
	}

	@Override
	public void closeAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void put(TabbedView<FileContentModel, IFileContentController> v) {
		
		if(v == null)
			throw new IllegalArgumentException("parameter arg == null");
		
		v.setOnCloseRequest(e -> {
			close(v);
		});
		
		tabPane.getTabs().add(v);
		
	}
	
}
