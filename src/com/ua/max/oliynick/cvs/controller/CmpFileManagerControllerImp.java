package com.ua.max.oliynick.cvs.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TabPane;

import com.ua.max.oliynick.cvs.interfaces.ICmpContentManagerModel;
import com.ua.max.oliynick.cvs.interfaces.ICmpFileContentController;
import com.ua.max.oliynick.cvs.interfaces.ICmpFileContentModel;
import com.ua.max.oliynick.cvs.interfaces.ICmpFileManagerController;
import com.ua.max.oliynick.cvs.view.TabbedView;

public class CmpFileManagerControllerImp extends ICmpFileManagerController implements Initializable {
	
	@FXML private TabPane tabPane;
	
	public CmpFileManagerControllerImp() {
		super();
	}
	
	public CmpFileManagerControllerImp(ICmpContentManagerModel model) {
		super();
		super.setModel(model);
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		/*for(TabbedView<FileContentModel, FileContentController> view : getModel().getContent()) {
			tabPane.getTabs().add(new Tab(view.getTitle(), view.getRoot()));
		}*/
		
	}

	@Override
	public boolean open(int indx) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean open(
			TabbedView<ICmpFileContentModel, ICmpFileContentController> v) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean close(int indx) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean close(
			TabbedView<ICmpFileContentModel, ICmpFileContentController> v) {
		return getModel().close(v);
	}

	@Override
	public void closeAll() {
		// TODO Auto-generated method stub

	}

	@Override
	public void put(
			TabbedView<ICmpFileContentModel, ICmpFileContentController> v) {
		// TODO Auto-generated method stub
		if(v == null)
			throw new IllegalArgumentException("parameter arg == null");
		
		v.setOnCloseRequest(e -> {
			close(v);
		});
		
		tabPane.getTabs().add(v);
	}

}
